# Shape detection and type classification

This repository contains two python script:

1. shape_detection.py - It takes in image folder path as command parser argument or by default checks for the folder images in the root folder and classifies images and displays any identified circles if any for circle type images or none for hexagon type images. It also stores the results in a text file outputs.txt

2. type_classification.py - This classifies images into two classes. The folder structure required to run this code is specified in the Documents folder under Report.pdf


## Required python packages for this repository
* ```numpy```
* ```opencv-python (cv2)```
* ```argsparse (default python library)``` 
* ```os (default python library)``` 
* ```torch (pytorch - only for type classification)``` 
* ```torchvision (pytorch - only for type classification)```


## Running the code:

Ensure your are inside the repository folder in the command line. 

Example:
~~~
python shape_classification.py -p ./shape_identification_images/
~~~ 


If the images are in different folder, please change the path to a different location in the command line prompt

For type classification running the code directly should be fine if the dataset "classification_images" is already present in the root folder in the approrpiate fashion.

~~~
python type_classification.py
~~~ 

## Python coding convention used
Style: PEP 8

Docstring type: Google docstring

## Report

The main report is available in the Document folder under Report.pdf which explains the code and design implementation.

For any questions please send an email to sreenivas.nm5493@gmail.com
