"""
.. module:: Assignment for finding a specific shape in an image based
   :platform: Unix
   :synopsis: This module finds the shape of object in an image.

.. module author:: Sreenivas Narasimha Murali (sreenivas.nm5493@gmail.com)

"""

import numpy as np
import cv2
import os
from argparse import ArgumentParser


class ShapeDetection:
    """
        This class deals with identifying shape of the structure in question and appropriate measures such as angle
        computation are performed.
    """
    def __init__(self, path):
        """
        Class initialization
        Args:
            path: string type. stores image folder path for accessing and processing images in the folder
        """
        self.path = path

    def __rgb2gray(self, img):
        """
        Converts RGB to gray-channel image
        Args:
            img: N-D uint8 array. input 3 channel image

        Returns:
            img_gray: N-D float32 array. single channel gray-scale image

        """
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return img_gray

    def __identify_shape(self, img, img_gray):
        """
        Identifies shape of the object in context using circle hough transform
        Args:
            img: N-D uint8 array. input 3 channel image
            img_gray: N-D float32 array. single channel gray-scale image

        Returns:
            output: N-D uint8 array. Shape identified 3 channel output image
            shape: string type. indicates shape
        """
        # These parameters need to be optimized better. Plotting false positive vs mis detection curves
        # would help further in this regard for future optimization for varying parameters over the entire dataset.

        # Grid search over all parameters is another way
        circles = cv2.HoughCircles(img_gray, cv2.HOUGH_GRADIENT, 1.1, 20, param1=98, param2=99,
                                   minRadius=60, maxRadius=150)
        output = img.copy()
        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            circles_selected = np.round(circles[0, :]).astype("int")

            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circles_selected:
                # draw the circle in the output image, then draw a rectangle
                # corresponding to the center of the circle
                cv2.circle(output, (x, y), r, (0, 255, 0), 4)
                cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

            shape = "circle"
            # show the output image
            cv2.imshow("output. press any key to proceed", np.hstack([img, output]))
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        else:
            shape = "hexagon"
            # show the output image
            cv2.imshow("output. press any key to proceed", np.hstack([img, output]))
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            pass
        return output, shape

    def shapetype(self):
        """
        Runs over all the images in a folder and generates a text file where it prints out image name and shape type
        Returns:

        """
        directory = self.path
        for filename in os.listdir(directory):
            if filename.endswith(".bmp"):
                img = cv2.imread(os.path.join(directory, filename))
                img_gray = self.__rgb2gray(img)
                output, shape = self.__identify_shape(img, img_gray)
                f = open('output.txt', 'a')
                f.write('\n')
                f.write(str(filename))
                f.write('\t')
                f.write(str(shape))
                f.close()
                continue
            else:
                continue


# running the code: python shape_detection.py --path image-path
if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-p', '--path',
                        help='image folder location for reading images',
                        type=str, default="./shape_identification_images/")
    args = parser.parse_args()
    shape_detection = ShapeDetection(args.path)
    shape_detection.shapetype()
