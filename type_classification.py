"""
.. module:: Assignment for classifiying two types of contextual objects
   :platform: Unix
   :synopsis: This module classifies images based on a simple neural network.

.. module author:: Sreenivas Narasimha Murali (sreenivas.nm5493@gmail.com)

"""

import torch.nn as nn
import torch.nn.functional as f
import torch.optim as optim
import torch
import torchvision
import torchvision.transforms as transforms


class TypeClassification(nn.Module):
    """
        This class deals with classification of two objects using a simple neural network architecture
    """
    def __init__(self):
        """
        Class initialization with all convolution, FCN and pooling layers initialized
        """
        super(TypeClassification, self).__init__()

        self.conv1 = nn.Conv2d(3, 4, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(4, 2, 5)
        # the below input size is pre-computed for image size of 300x200 after two valid convolutions of stride 5
        # This can be fixed once we know image size
        self.fc1 = nn.Linear(2 * 47 * 72, 40)
        self.fc2 = nn.Linear(40, 10)
        self.fc3 = nn.Linear(10, 2)

    def forward(self, img_input):
        """
        Forward pass of the network overloaded on the default NN.module
        Args:
            img_input: Input image batch N-D array

        Returns:
            output: Probability map in [0,1] range for two class problems
        """
        layer1 = self.pool(f.relu(self.conv1(img_input)))
        layer2 = self.pool(f.relu(self.conv2(layer1)))
        layer2 = layer2.view(-1, 2 * 47 * 72)
        layer3 = f.relu(self.fc1(layer2))
        layer4 = f.relu(self.fc2(layer3))
        output = self.fc3(layer4)
        return output


if __name__ == "__main__":
    # initialize the model, loss function and optimizer
    model = TypeClassification()
    loss_criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

    # load datasets into train and test
    trainset = torchvision.datasets.ImageFolder(root='./classification_images/train/',
                                                transform=transforms.Compose(
                                                    [transforms.Resize((200, 300)), transforms.ToTensor()]))
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=4, shuffle=True)
    testset = torchvision.datasets.ImageFolder(root='./classification_images/test/',
                                               transform=transforms.Compose(
                                                   [transforms.Resize((200, 300)), transforms.ToTensor()]))
    testloader = torch.utils.data.DataLoader(testset, batch_size=4, shuffle=True)

    # check if cuda is available for gpu computations
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.to(device)
    # train the network
    for epoch in range(10):

        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels = data

            # send input and labels to gpu too if available
            inputs, labels = inputs.to(device), labels.to(device)
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            loss = loss_criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            print('[epoch: %d,batch %d] loss: %.3f' %(epoch + 1, i + 1, running_loss))
            running_loss = 0.0

    print('Training done')

    # test the network

    correct = 0
    total = 0
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            images, labels = images.to(device), labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    print('Accuracy of the network on the test images: %f %%' % (100 * correct / total))
